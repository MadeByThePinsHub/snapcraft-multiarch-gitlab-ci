## Snapcraft Multiarch Gitlab-CI

This repo contains a build script for GitLab-CI. You may use this setup by adding the following to your `.gitlab-ci.yml` config:

```yaml
include:
  - project: diddledan/snapcraft-multiarch-gitlab-ci
    ref: main
    file: /snapcraft.yml
  - project: diddledan/snapcraft-multiarch-gitlab-ci
    ref: main
    file: /snapcraft-architectures.yml

amd64:
  extends:
    - .snapcraft
    - .amd64
i386:
  extends:
    - .snapcraft
    - .i386
arm64:
  extends:
    - .snapcraft
    - .arm64
armhf:
  extends:
    - .snapcraft
    - .armhf
ppc64el:
  extends:
    - .snapcraft
    - .ppc64el
```

You may customise the build with the following [variables](https://docs.gitlab.com/ee/ci/variables/):

| Variable | Description |
|-|-|
|SNAPCRAFT_PROJECT_ROOT|The directory containing your Snapcraft project. This is the directory containing either `snap/snapcraft.yaml`, `snapcraft.yaml` (without a `snap` directory at all), or `.snapcraft.yaml`|
|SNAPCRAFT_BUILD_INFO|Wether to include metadata about how the build was performed into the final snap. Proprietary application vendors might prefer this is disabled. May be either `1` to enable or `0` to disable. Default is enabled.|
|USE_SNAPCRAFT_CHANNEL|Override the snapcraft channel to install a non-default version of `snapcraft` to build with.|
|SNAPCRAFT_PARAMETERS|Extra parameters to pass on the `snapcraft` command line.|
|ARCHITECTURE|The architecture to build. This is usually set with the `extends` mechanism with the `.amd64`, `.i386`, etc templates|
